package twitter.dao.mapper;

import org.apache.ibatis.annotations.Param;

import twitter.entity.User;
import twitter.form.SignUpForm;

public interface UserMapper {

	public void userRegister(SignUpForm form);

	public User login(LoginForm form);


}



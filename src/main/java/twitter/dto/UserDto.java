package twitter.dto;

public class UserDto {

		private int id;
		private String name;
		private String account;
		private String password;
		private String email;
//		private Date createdDate;
//		private Date updatedDate;

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAccount() {
			return account;
		}
		public void setAccount(String account) {
			this.account = account;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
//		public Date getCreatedDate() {
//			return createdDate;
//		}
//		public void setCreatedDate(Date createdDate) {
//			this.createdDate = createdDate;
//		}
//		public Date getUpdatedDate() {
//			return updatedDate;
//		}
//		public void setUpdatedDate(Date updatedDate) {
//			this.updatedDate = updatedDate;
//		}
	}


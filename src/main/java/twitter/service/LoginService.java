package twitter.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import twitter.dao.mapper.UserMapper;
import twitter.entity.User;
import twitter.form.LoginForm;
@Service
public class LoginService {
	@Autowired
    private static UserMapper userMapper;

	public static String login(LoginForm form){
		String message = null;
		User entity = userMapper.login(form);
		if(entity != null) {
			message = ("ログインしました");
		} else {
			message = ("ログインに失敗しました");
		}
		return message;

	}

}

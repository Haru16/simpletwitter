package twitter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import twitter.dao.mapper.UserMapper;
import twitter.form.SignUpForm;


@Service
public class UserService {

	@Autowired
    private UserMapper userMapper;

	public void userRegister(SignUpForm form){

		userMapper.userRegister(form);
	}
}




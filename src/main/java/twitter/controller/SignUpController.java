package twitter.controller;

import javax.validation.Valid;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import twitter.form.SignUpForm;
import twitter.service.UserService;

public class SignUpController {

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signUp(Model model) {
		SignUpForm form = new SignUpForm();
		model.addAttribute("title","登録");
		model.addAttribute("signUpForm", form);
		model.addAttribute("message", "登録してね(*'▽')☆");
		return "insert";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String valid(@Valid @ModelAttribute SignUpForm form, BindingResult result, Model model) {
	    if (result.hasErrors()) {
	        model.addAttribute("title", "エラー");
	        model.addAttribute("message", "以下のエラーを解消してね(´;ω;｀)");
	    } else {
	    	UserService.userRegister(form);
	    	model.addAttribute("message", "登録しました");
	    	model.addAttribute("userForm", form);

	    }
		return "insert";
	}
}




package twitter.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import twitter.form.LoginForm;
import twitter.service.LoginService;

@Controller
public class LoginController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
		return "login";
	    }

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@Valid @ModelAttribute LoginForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("title", "エラー");
	        model.addAttribute("message", "以下のエラーを解消してね(´;ω;｀)");
		} else {
				String message = LoginService.login(form);
				model.addAttribute("message",message);
		}
        return "login";
	}
}


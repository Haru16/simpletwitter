<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="${title}"></c:out></title>
</head>
<body>
	<h2>新規登録</h2>
	<form:form modelAttribute="signUpForm">
		<table>
			<tbody>
			<tr>
				<td><form:label path="name">名前</form:label></td>
				<td><form:input path="name" size="20" /></td>
			</tr>
			<tr>
				<td><form:label path="account">アカウント名</form:label></td>
    			<td><form:input path="account" size="20" /></td>
			</tr>
			<tr>
				<td><form:label path="pw">パスワード</form:label></td>
    			<td><form:input path="pw" size="20" /></td>
			</tr>
			<tr>
				<td><form:label path="email">メールアドレス</form:label></td>
   			 	<td><form:input path="email" size="20" /></td>
			</tr>
   		 	<tr>
   				 <td><form:label path="discription">説明</form:label></td>
    				<td><form:textarea path="discription" cols="20" row="5" /></td>
    		</tr>
    	</tbody>
      </table>
      <input type="submit" />
    </form:form>

    <c:if test="${not empty employeeList}">
    	<table border="1">
            <tbody>
                <tr>
                    <th>名前</th>
                    <th>アカウント名</th>
                    <th>パスワード</th>
                    <th>メールアドレス</th>
                    <th>説明</th>
                </tr>
                <c:forEach var="employee" items="${userForm}">
                    <tr>
                        <td><c:out value="${userForm.name}"></c:out></td>
                        <td><c:out value="${userForm.account}"></c:out></td>
                        <td><c:out value="${userForm.pw}"></c:out></td>
                        <td><c:out value="${userForm.email}"></c:out></td>
                        <td><c:out value="${userForm.discription}"></c:out></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:if>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessage">
				<ul>
					<c:forEach items="${errorMessages}" var="messe">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<form:form modelAttribute="loginForm">
			<form:label path="account">アカウント名かメールアドレス</form:label>
            <form:input path="email" /> <br />

            <form:label path="password">パスワード</form:label>
            <form:input path="password" type="password" /> <br />

            <input type="submit" value="ログイン" /> <br />
            <a href="./">戻る</a>
        </form:form>
	</div>
</body>
</html>